/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea.pkg3;
import java.util.Scanner;
/**
 *
 * @author macab
 */
public class Salario {
    public static void salario(){
        double horasLaboradas, precioHora, salarioBruto, salarioNeto, deducciones;
        
        Scanner var = new Scanner(System.in);
        System.out.println("Digite la cantidad de horas laboradas en el mes: ");
        horasLaboradas = var.nextDouble();
        System.out.println("Digite el precio por hora: ");
        precioHora = var.nextDouble();
        salarioBruto = horasLaboradas * precioHora;
        salarioNeto = salarioBruto - (salarioBruto * (9.17/100));
        deducciones = salarioBruto - salarioNeto;
        System.out.println("El salario bruto es: "+ salarioBruto + " El salario neto es: "+ salarioNeto+ " Las deducciones son: " +deducciones);
    }
    
}
