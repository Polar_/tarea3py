/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea.pkg3;

import java.util.Scanner;
import java.util.InputMismatchException;
/**
 *
 * @author macab
 */
public class Tarea3 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner sn = new Scanner(System.in);
        boolean salir = false;
        int op; 
       while (!salir) {

            System.out.println("(1). (Calcular la Edad Actual)");
            System.out.println("(2). (Salario)");
            System.out.println("(3). (Numeros hasta 20)");
            System.out.println("(4). (Promedios Notas)");
            System.out.println("(5). (SALIR)");

            try {

                System.out.println("Elega una opción: ");
                op = sn.nextInt();

                switch (op) {
                    case 1:
                        Edad.Edad();
                        break;
                    case 2:
                       Salario.salario();
                        break;
                    case 3:
                        numerosHasta20.Numeros();
                        break;
                    case 4:
                        PromedioNota.PromedioNotas();
                        break;
                    case 5:
                        salir = true;
                        break;
                    default:
                        System.out.println("ingrese una opcion valida");
                }
            } catch (InputMismatchException e) {
                System.out.println("Debes insertar un número valido");
                sn.next();
            }
        }
        }
}
    
